const WalltureError = require('../utils/WalltureError');

/**
 * Custom error which extends WalltureError class.
 * It is responsible to throw Bad request error with customized message.
 */
class BadRequestError extends WalltureError {
  /**
   * Constructor which takes message for the error.
   * @param {String} message
   */
  constructor(message) {
    super(message || 'Bad request.', 400);
  }
}

module.exports = BadRequestError;
