const WalltureError = require('../utils/WalltureError');

/**
 * Custom error which extends WalltureError class.
 * It is responsible to throw a custom error, MongoDB Casting Exception.
 */
class MongoCastError extends WalltureError {
  /**
   * Constructor which takes message for the error.
   * @param {String} message
   */
  constructor(message) {
    super(message || 'Could not cast given input for MongoDB', 422);
  }
}

module.exports = MongoCastError;
