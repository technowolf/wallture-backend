const mongoose = require('mongoose');

const {Schema} = mongoose;

/**
 * Schema to be used to define the wallpaper object.
 * @type {module:mongoose.Schema<Document, Model<Document>>}
 */
const wallpaperSchema = new Schema({
  // Name of the Wallpaper
  name: {type: String},
  // List of tags associated with the wallpaper
  tags: [String],
  // List of categories to which the wallpaper belongs
  category: [String],
  // Author Metadata
  author: {
    // Author Metadata: Name of the Author of the wallpaper
    authorName: {type: String},
    // Author Metadata: URL to the portfolio/webpage/social media of the Author
    authorUrl: {type: String},
  },
  /**
   *  A copyright text if image was taken from unsplash or similar sites.
   *  Though sites must provide the image as CC license. make sure of that.
   */
  copyleft: String,
  // A resolvable URL for the image to feed it to the client apps.
  url: {type: String},
  // No. of likes associated with this wallpaper. Default: 0
  likes: {
    type: Number, default: 0,
  },
  // No. of downloads associated with this wallpaper. Default: 0
  downloads: {
    type: Number, default: 0,
  },
  // Boolean flag to specify if the wallpaper is AMOLED Ready or not.
  isAmoled: {type: Boolean, default: false},
  // Boolean flag to specify if the wallpaper is associated with anime/cartoon.
  isAnime: {type: Boolean, default: false},
});

/**
 * Export wallpaper model schema
 * @type {Model<Document>}
 */
module.exports = mongoose.model('Wallpapers', wallpaperSchema);
