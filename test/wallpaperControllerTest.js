// Dependencies
require('should');
const sinon = require('sinon');
const wallpaperController = require('../controllers/wallpaperController');

// Testing Controller for denying POST with missing required fields.
describe('Wallpaper Controller Test', () => {
  describe('POST', () => {
    it('should not allow empty name, URL, authorUrl, authorName on POST.',
        () => {
          const Wallpaper = function(wallpaper) {
            this.save = () => {
            };
          };
          const req = {
            body: {
              author: {},
            },
          };
          const res = {
            status: sinon.spy(),
            send: sinon.spy(),
            json: sinon.spy(),
          };
          const controller = wallpaperController(Wallpaper);
          controller.post(req, res);

          res.status.calledWith(400).
              should.
              equal(true, `Bad status ${res.status.args[0][0]}`);
        });
  });
});
