// This Controller will handle GET, POST requests.
const BadRequestError = require('../utils/BadRequestError');

/**
 * Controller for Users route which is responsible to facilitate with
 * GET and POST request for /wallpaper path.
 * @param {WallpaperSchema} Wallpaper
 * @return {{post: (function(*, *): (*)), get: get}}
 */
function wallpaperController(Wallpaper) {
  /**
   * GET Handler
   * @param {Request} req
   * @param {Response} res
   */
  function get(req, res) {
    /**
     * Preparing a query using parameters in request.
     * Defaults to {}, returning all wallpapers.
     * @type {Json}
     */
    const query = {};
    // If searched with name=
    if (req.params.name) query.name = req.params.name;
    // If searched with tag=
    if (req.params.tag) query.tags = req.params.tags;
    // If searched with category=
    if (req.params.category) query.category = req.params.category;
    // If searched with authorName=
    if (req.params.authorName) query.authorName = req.params.authorName;

    // This will run the query constructed above.
    Wallpaper.find(query, (err, wallpapers) => {
      // Returns any kind of error if it occurs.
      if (err) return res.send(err);
      if (wallpapers.length <= 0) {
        return res.json({message: 'No wallpapers found.'});
      }
      const returnWallpapers = wallpapers.map((wallpaper) => {
        // Parses wallpapers into links to individual
        const newWallpaper = wallpaper.toJSON();
        newWallpaper.links = {};
        newWallpaper.links.self =
            `http://${req.headers.host}/wallpapers/${wallpaper._id}`;
        return newWallpaper;
      });
      // Return the wallpapers.
      return res.status(200).json(returnWallpapers);
    });
  }

  /**
   * POST Handler
   * @param {Request} req
   * @param {Response} res
   * @return {*}
   */
  function post(req, res) {
    let err = null;
    // Checking for Wallpaper Name.
    if (!req.body.name) {
      err = new BadRequestError('Wallpaper Name is required.');
    }
    // Checking for Wallpaper URL.
    if (!req.body.url) {
      err = new BadRequestError('URL is required.');
    }
    // Checking for author.authorName in body.
    if (!req.body.author.authorName) {
      err = new BadRequestError('Author object must have Author Name.');
    }
    // Checking for author.authorUrl in body.
    if (!req.body.author.authorUrl) {
      err = new BadRequestError('Author object must have Author url.');
    }

    if (err !== null) {
      res.status(err.status);
      return res.send(err);
    }

    // Build a Wallpaper document if everything is satisfied.
    const wallpaper = new Wallpaper(req.body);
    // Save the document on DB.
    wallpaper.save();
    // Return 201, with the wallpaper document just created.
    res.status(201);
    return res.json(wallpaper);
  }

  // Return both, GET and POST handlers.
  return {post, get};
}

module.exports = wallpaperController;
